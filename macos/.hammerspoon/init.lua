hyper = { "cmd", "alt", "shift", "ctrl" }
k = hs.hotkey.modal.new({}, 'F18')
hyperBindings = {'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','space', '[', ']', '\\', 'delete', 'up', 'down', 'left', 'right'}

for i,key in ipairs(hyperBindings) do
    k:bind({}, key, nil, function() hs.eventtap.keyStroke(hyper, key)
        k.triggered = true
    end)
end

-- Enter Hyper Mode when F19 is pressed
pressedF19 = function()
    k.triggered = false
    k:enter()
end

-- Leave Hyper Mode when F19 is pressed
releasedF19 = function()
    k:exit()
end

-- Bind the Hyper key
f19 = hs.hotkey.bind({}, 'F19', pressedF19, releasedF19)


-- Automatic Reloading
function reloadConfig(files)
    doReload = false

    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end

    if doReload then
        hs.reload()
    end
end

