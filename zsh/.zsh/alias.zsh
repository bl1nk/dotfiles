alias sudo='sudo '
alias s='sudo '
alias v='vim '
alias ta='tmux a -t'
alias td='tmux detach'
alias g='git '

alias ls='ls -sh --color=auto'
alias l='ls -l'
alias ll='l'
alias lt='ls -tr'
alias llt='l -tr'
alias la='ls -a'
alias lla='ll -a'

alias grep='grep --color'

alias -g T="| tail"
alias -g G="| grep"
alias -g H="| head"

alias mkdir='nocorrect mkdir'
alias wget='nocorrect noglob wget'
alias wgetc="wget --content-disposition"

alias tm="if tmux has; then tmux attach -d; else tmux new; fi"
